package com.hw.db.controllers;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.*;
import com.hw.db.models.Thread;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class threadControllerTests {
    private Thread testThread;
    private List<Post> emptyPosts;
    private User testUser;

    private int threadId = 0;

    @BeforeEach
    @DisplayName("Prerequirements")
    void prerequirements() {
        emptyPosts = Collections.emptyList();
        testThread = new Thread("Denis", new Timestamp(2121348928345L), "TestForum", "TestMessage", "TestSlug", "TestTitle", 21);
        testThread.setId(threadId);
        testUser = new User("nickname", "email", "about", "Denis Levkovets");
    }


    @Test
    @DisplayName("Receive Forum")
    void receiveForum() {
        try (
            MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)
            ) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("TestSlug"))
                    .thenReturn(testThread);

            threadMock.when(() -> ThreadDAO.getThreadById(threadId))
                    .thenReturn(testThread);

            try (
                MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class)
                ) {
                threadController controller = new threadController();

                assertEquals(testThread, controller.CheckIdOrSlug(threadId.toString()), "Thread returned successfully");
            }
            assertEquals(testThread, ThreadDAO.getThreadBySlug("TestSlug"));
            assertEquals(testThread, ThreadDAO.getThreadById(threadId));
        }
    }

    @Test
    @DisplayName("Create Forum")
    void createForum() {
        try (
            MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)
            ) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("TestSlug"))
                    .thenReturn(testThread);

            threadMock.when(() -> ThreadDAO.getThreadById(threadId))
                    .thenReturn(testThread);

            try (
                MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class)
                ) {
                threadController controller = new threadController();

                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(emptyPosts), controller.createPost("new post", emptyPosts), "Post created successfully");
            }
            assertEquals(testThread, ThreadDAO.getThreadBySlug("TestSlug"));
            assertEquals(testThread, ThreadDAO.getThreadById(threadId));
        }
    }

    @Test
    @DisplayName("Get empty posts list")
    void getEmptyPostsList() {
        try (
            MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)
            ) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("TestSlug"))
                    .thenReturn(testThread);

            threadMock.when(() -> ThreadDAO.getThreadById(threadId))
                    .thenReturn(testThread);

            try (
                MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class)
                ) {
                threadController controller = new threadController();

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(Collections.emptyList()), controller.Posts("TestSlug", 5, 0, "testSort", true), "");
            }
            assertEquals(testThread, ThreadDAO.getThreadBySlug("TestSlug"));
            assertEquals(testThread, ThreadDAO.getThreadById(threadId));
        }
    }

    @Test
    @DisplayName("Get empty posts list from existed thread")
    void getThreadEmptyPostsList() {
        try (
            MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)
            ) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("TestSlug"))
                    .thenReturn(testThread);

            threadMock.when(() -> ThreadDAO.getThreadById(threadId))
                    .thenReturn(testThread);

            try (
                MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class)
                ) {
                threadController controller = new threadController();

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(testThread), controller.change("TestSlug", testThread));
            }
            assertEquals(testThread, ThreadDAO.getThreadBySlug("TestSlug"));
            assertEquals(testThread, ThreadDAO.getThreadById(1));
        }
    }

    @Test
    @DisplayName("Get post info")
    void getPostInfo() {
        try (
            MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)
            ) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("TestSlug")).thenReturn(testThread);

            threadController controller = new threadController();

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(testThread), controller.info("TestSlug"));
        }
    }

    @Test
    @DisplayName("Create post vote")
    void createVote() {
        try (
            MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)
            ) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("TestSlug"))
                    .thenReturn(testThread);

            threadMock.when(() -> ThreadDAO.getThreadById(threadId))
                    .thenReturn(testThread);

            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                threadController controller = new threadController();
                userMock.when(()->UserDAO.Info("Some info")).thenReturn(testUser);
                Vote testVote = new Vote("votes", 2);

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(testThread), controller.createVote("TestSlug", testVote));
            }
            assertEquals(testThread, ThreadDAO.getThreadBySlug("TestSlug"));
            assertEquals(testThread, ThreadDAO.getThreadById(threadId));
        }
    }
}